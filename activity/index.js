   let username;
   let password;
   let role;

   function isLogIn(user, password, role){
    username = prompt('Enter your username:');
    password = prompt('Enter your password:');
    role = prompt('Enter your role:');
    if (username === '' || password === '' || role === ''){
        alert('Input must not be empty!');
    }
    else {
        switch (role) {
            case 'admin':
            alert("Welcome back to the class portal, admin!");
            break;
            case 'teacher':
            alert("Thank you for logging in, teacher!");
            break;
            case 'rookie':
            alert("Welcome to the class portal, student!");
            break;
            default:
            alert("Role out of range.");
        }
    }
    return role;
}

const role2 = isLogIn(role);




function checkAverage(num1,num2,num3,num4){
    let average = Math.round((num1 + num2 + num3 + num4)/4);

    if (role2 === 'admin' || role2 === 'teacher' || role2 === ''){
        alert(`${role2}! You are not allowed to access this feature!`)
    }
    else{
        if (average <= 74){
            console.log(`Hello, student, your average is ${average}. The letter equivalent is F`)
        }else if (average >= 75 && average <= 79){
            console.log(`Hello, student, your average is ${average}. The letter equivalent is D`)
        }else if (average >= 80 && average <= 84){
            console.log(`Hello, student, your average is ${average}. The letter equivalent is C`)
        }else if (average >= 85 && average <= 89){
            console.log(`Hello, student, your average is ${average}. The letter equivalent is B`)
        }else if (average >= 90 && average <= 95){
            console.log(`Hello, student, your average is ${average}. The letter equivalent is A`)
        }else{
            console.log(`Hello, student, your average is ${average}. The letter equivalent is A+`)
        }
    }
}
